# A library for location visualization

import folium

folium.Map(location=[31.2389, 121.4992])

## 初始化地图，指定上海市
m = folium.Map(
    location=[31.2389, 121.4992],
    zoom_start=14
)

# 中文转换
def parse_zhch(s):
    return str(str(s).encode('ascii' , 'xmlcharrefreplace'))[2:-1]

# 悬浮弹出信息
tooltip = parse_zhch('嘿！')

# 添加marker到地图
folium.Marker([31.2453,121.4857], popup=parse_zhch('外白渡桥'), tooltip=tooltip).add_to(m)
folium.Marker([31.2418,121.4953], popup=parse_zhch('东方明珠'), tooltip=tooltip).add_to(m)

m

## 初始化地图，指定上海市，添加互动标记！
m.add_child(folium.ClickForMarker(popup='hey'))

m

# in case for discrete data
# 笔记--气温可视化
import csv
from matplotlib import pyplot as plt

filename = 'sitka_weather_07-2014.csv'

#从文件中获取最高气温
with open(filename) as f:
    reader = csv.reader(f)
    header_row = next(reader)
    highs = []
    for row in reader:
        high = int(row[1])
        highs.append(high)

#根据数据绘制图形
fig = plt.figure(dpi=128, figsize=(10,6))
plt.plot(highs, c="red")

#设置图形的格式
plt.title("Daily high temperatures, July 2014", fontsize = 24)
plt.xlabel("", fontsize = 16)
plt.ylabel("Temperature(F)", fontsize = 16)
plt.tick_params(axis="both", which = "major", labelsize = 16)

plt.show()
