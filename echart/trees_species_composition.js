option = {
  title: {
    text: 'Trees species composition in LA GOCCIA',
    
    left: 'center'
  },
  tooltip: {
    trigger: 'item'
  },
  legend: {
    orient: 'vertical',
    left: 'left'
  },
  series: [
    {
      name: 'Access From',
      type: 'pie',
      radius: '50%',
      data: [
        { value: 34.8, name: 'Robinia pseudoacacia' },
        { value: 29.5, name: 'Celtis australis' },
        { value: 8.1, name: 'Populus nigra' },
        { value: 7.7, name: 'Ulmus minar' },
         { value: 5.7, name: 'Sambucus nigra' },
          { value: 3.3, name: 'Prinis avium' },
           { value: 2.0, name: 'Ailanthus altissima' },
              { value: 5.55, name: 'Others' },
        { value: 1.0, name: 'Ficus carica' }
      ],
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)'
        }
      }
    }
  ]
};
