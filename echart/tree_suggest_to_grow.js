option = {
  tooltip: {

    trigger: 'axis',
    
    axisPointer: {
      // Use axis to trigger tooltip
      type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
      
      
    }
  },
  legend: {
     "textStyle": {
            "fontSize": 15
        }
  },
  label:{
     "textStyle": {
            "fontSize": 100
        }
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
    
   
     
  },
  xAxis: {
    type: 'value'
   
  },
  yAxis: {
    type: 'category',
    data: ['Black poplar', 'Black locust', 'Field elm', 'Robinia', 'European hackberry']
  },
  series: [
    {
      name: 'Oxygen-release',
      type: 'bar',
      stack: 'total',
      label: {
        show: true
      },
      emphasis: {
        focus: 'series'
      },
      data: [37.33, 68.45,37.33,1.82,13.87]
    },
    {
      name: 'Carbon storage',
      type: 'bar',
      stack: 'total',
      label: {
        show: true
      },
      emphasis: {
        focus: 'series'
      },
      data: [4.4,1.3,2.3,0.3,0.2]
    },
    {
      name: 'Carbon sequestration',
      type: 'bar',
      stack: 'total',
      label: {
        show: true
      },
      emphasis: {
        focus: 'series'
      },
      data: [56,25,12,5,5]
    },
    
  ]
};
