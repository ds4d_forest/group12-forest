
var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};

var option;



option = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      // Use axis to trigger tooltip
      type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
    }
  },
  legend: {},
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis: {
    type: 'value'
  },
  yAxis: {
    type: 'category',
    data: ['Buddleja davidii', 'Ficus carica', 'Prunus domestica', 'Crataegus monogyna', 'Morus nigra', 'Prunus avium', 'Ulmus', 'Celtis','Fraxinus latifolia','Celtis austrails','Ailanthus altissima','Robinia','Robinia visosa','Robinia pseudoacaia','Acer campestre','Ulmus minor','Acer negundor','Populus nigra','Tilia cordata','Platanus']
  },
  series: [
    {
      name: 'Net Carbon Sequestration',
      type: 'bar',
      stack: 'total',
       color: 'rgb(242, 115, 128)',
      label: {
        show: false
    
      },
      emphasis: {
        focus: 'series'
      },
      data: [0.003,0.004,0.004,0.002,0.002,0.002,0.006,0.001,0.002,0.0008,0.006,0.005,0.009,0.004,0.001,0.011,0.006,0.011,0.01,0.013]
    },
    {
      name: 'Oxygen-release',
      type: 'bar',
      color: 'rgb(73, 215, 251)',
      stack: 'total',
      label: {
        show: false
      },
      emphasis: {
        focus: 'series'
      },
      data: [0.006,0.008,0.009,0.0045,0.005,0.006,0.02,0.0035,0.0055,0.0025,0.017,0.015,0.024,0.012,0.003,0.026,0.02,0.028,0.026,0.0365]
    },
    {
      name: 'Leaf Area',
      type: 'bar',
      stack: 'total',
        color: 'rgb(18, 165, 111)',
      label: {
        show: false
      },
      emphasis: {
        focus: 'series'
      },
      data: [,,,,,,,0.0028,,0.006,,,,0.006,0.015,,,0.003,0.014,0.013]
    },
   
   
  ]
};

if (option && typeof option === 'object') {
    myChart.setOption(option);
}

        
